# MASS SCRIMMAGE Benchmarks

James K Lewis<sup>1,2</sup>, Kevin J DeMarco<sup>3</sup>, and Frazier N Baker<sup>1</sup><br>
_<sup>1</sup>Advanced Concepts Laboratory, Georgia Tech Research Institute, Fairborn, OH;_<br>
_<sup>2</sup>The George W. Woodruff School of Mechanical Engineering, Georgia Institute of Technology, Atlanta, GA;_<br>
_<sup>3</sup>Aerospace Transporation and Advanced Systems Laboratory, Georgia Tech Research Institute, Atlanta, GA;_

## Introduction
 

SCRIMMAGE is a piece of software that fascilitates multi-agent simulations. [1] The various aspects of each agent's behavior, like autonomy, controllers, and motion models, are implemented as plugins with a high degree of interchangeability. This allows researchers to focus their efforts on developing the plugins most relevant to their research, while using existing plugins for everything else. This modular approach can be a powerful asset for users with the appropriate experience, but building and utilizing SCRIMMAGE may require time and knowledge beyond what might be expected from a student. Pre-configured Docker containers help mitigate this problems in that a student has a single piece of software to install with no additional setup, but often at the cost of reduced performance due to virtualization. To assess the viabilty of this as an environment to run SCRIMMAGE, it is necessary to perform benchmarks. Demarco et al. performed benchmarks on SCRIMMAGE running natively, recording the ratio of simulation time to wall time. [1] Similarly, the benchmarks conducted for this report will find this ratio for SCRIMMAGE running in a virtualized environment on a student laptop. Additionally, RAM and CPU usage will be reported. 

## Methods

A Docker image based on "multiplatformautonomy:vscrimmage-vnc" was created to provide a consistent platform for benchmarking. The Docker image uses the SCRIMMAGE version whose git hash begins with "cb566bc". Benchmarks were performed on a laptop running Docker for Windows on Windows 10 with an Intel i7-8550U CPU and 16 GB of RAM. The "predator_prey_boids" mission was used for all benchmarks.

Each benchmark trial records SCRIMMAGE's RAM usage, system CPU usage by core, and the wall time taken to perform the simulation. The benchmark begins recording CPU usage five seconds before SCRIMMAGE is started to provide a baseline measurement. CPU and RAM usage are measured at one second intervals and written to log files. The elapsed time is taken from a SCRIMMAGE log file.

Benchmark trials were conducted using the motion models Single Integrator, Unicycle, and JSBSim. The number of entities was set to 200, 500, 800, 1100, 1400, 1700, and 2000, and 5 trials were conducted at each set of conditions. This resulted in a total of 105 trials. The motion models and entity counts are the same as those used for the benchmarks performed by Demarco et al. and the mission, while different, still includes large numbers of entities using the "Boids" autonomy [1]. For each trial, the ratio of simulation time to wall time was found and compared to the respective benchmark from the aforementioned paper. The peak RAM usage was found for each trial and averaged for each set of conditions.  The CPU usage of all cores was summed for each trial. the average, median, and max of this time series was found, and these quantities were averaged across trials for each set of conditions. As such, CPU usage statistics are reported as a percentage, with a maximum value of 8 cores * 100% = 800%.       

## Results


Tables 1 through 5 present the ratio of simulation time to wall time, peak RAM usage, average CPU usage, median CPU usage, and max CPU usage, respectively. Figure 1 plots the ratio as a function of entity count. Figure 2 plots peak RAM usage as a function of entity count. 

 | Entity Count | Single Integrator | Unicycle | JSBSim |
 | ------------ | ----------------- | -------- | ------ |
 | 200          | 18.95             | 19.70    | 1.02   |
 | 500          | 6.27              | 6.89     | 0.40   |
 | 800          | 2.97              | 3.38     | 0.25   |
 | 1100         | 1.79              | 2.07     | 0.17   | 
 | 1400         | 1.18              | 1.41     | 0.13   | 
 | 1700         | 0.88              | 1.05     | 0.11   | 
 | 2000         | 0.64              | 0.79     | 0.09   | 

**Table 1.** The ratio of simulation time to wall time for each combination of motion model and entity count.

<br/>

 | Entity Count |Single Integrator | Unicycle | JSBSim |
 | ------------ | ---------------- | -------- | ------ |
 | 200          | 1.6              | 1.6      | 1.7    |
 | 500          | 2.0    		   | 2.0      | 2.4    |
 | 800          | 2.5              | 2.5      | 3.1    |
 | 1100         | 3.0    		   | 3.0      | 3.7    | 
 | 1400         | 3.4    		   | 3.5      | 4.4    |
 | 1700         | 4.0       	   | 3.9      | 5.2    |
 | 2000         | 4.7       	   | 4.5      | 5.8    |
  
**Table 2.** Peak RAM usage (GB) for each set of conditions.

<br/>

 | Entity Count | Single Integrator | Unicycle | JSBSim |
 | ------------ | ----------------- | -------- | ------ |
 | 200 			| 146.1				| 153.3	   | 170.1  |			
 | 500 			| 160.7				| 163.4    | 187.0  |
 | 800			| 169.2				| 173.4    | 187.7	|
 | 1100			| 175.8				| 178.1    | 188.6	|
 | 1400			| 179.5				| 182.3    | 189.0	|
 | 1700			| 184.2				| 184.4    | 189.6	|
 | 2000			| 186.1				| 186.3    | 189.8	|
 
**Table 3.** Average CPU usage (%, where full usage is number of cores\*100%) over the course of the trial.

<br/>

 | Entity Count | Single Integrator | Unicycle | JSBSim |
 | ------------ | ----------------- | -------- | ------ |
 | 200          | 167.7      		| 170.7    | 182.4  |
 | 500          | 174.4      		| 180.1    | 187.7  |
 | 800          | 181.3     		| 187.9    | 188.2  |
 | 1100         | 186.8      		| 191.0    | 189.2  |
 | 1400         | 188.7     		| 194.3    | 189.6  |
 | 1700         | 192.8      		| 196.1    | 190.3  |
 | 2000         | 194.6     		| 196.9    | 190.6  |
 
**Table 4.** Median CPU usage (%, where full usage is number of cores\*100%) over the course of the trial. 

<br/>

 | Entity Count | Single Integrator | Unicycle | JSBSim |
 | ------------ | ----------------- | -------- | ------ |
 | 200          | 184.7             | 192.3    | 216.7  |
 | 500          | 194.6             | 202.3    | 206.9  |
 | 800          | 196.9   			| 209.1    | 207.7  |
 | 1100         | 205.9 			| 209.1    | 209.6  |
 | 1400         | 209.4  			| 219.4    | 213.6  |
 | 1700         | 214.8 			| 219.4    | 213.4  |
 | 2000         | 215.1 			| 222.3    | 213.3  |

**Table 5.** Max CPU usage (%, where full usage is number of cores\*100%) over the course of the trial.

<br/>

![Ratio Plot](./results_processed/ratio_plot_cropped.png)

**Figure 1.** A plot of the ratio of simulation time to wall time as a function of entity count. 

<br/>

![Ram Usage Plot](./results_processed/ram_plot_cropped.png)

**Figure 2.** A plot of peak RAM usage as a function of entity count. 

## Discussion

The ratios of simulation time to wall time are relatively large when the two lower fidelity models are used with low entity counts. However, the ratios quickly decrease as entity count rises. Additionally, the JSBSim motion model was only able to run faster than real time with the lowest entity count. This suggests that SCRIMMAGE is capable of running much faster than real time on a student laptop, given that the entity count is kept low enough and high fidelity models aren't used. In the context of reinforcement learning for autonomy, this may make it necessary to conduct training using either lower fidelity motion models or higher performance computing resources. The entity count itself may not be as strict of a restriction, as SCRIMMAGE ran between 6 and 7 times faster than real time with an entity count of 500. 

The RAM usage of the two lower fidelity models appears similar, while the RAM usage of the JSBSim increases much faster with rising entity counts. For trials using the JSBSim motion model and an entity count of 2000, SCRIMMAGE had a peak RAM usage of 5.8 ± (CI PLACEHOLDER: 1.96 x s/sqrt(n)?) GB. With the same entity count, the Single Integrator and Unicycle motion models had peak RAM usages of 4.7 ± (CI PLACEHOLDER) GB and 4.5 ± (CI PLACEHOLDER) GB respectively. RAM usage may become a limiting factor for entity counts given that some college students may not be able to afford laptops with more than 8 GB of RAM.

The CPU usage did not exhibit as pronounced a trend as the other metrics. While average CPU usage did increase as the entity count was increased, the change was relatively small for all motion models. Of note is that the average CPU usage of the JSBSim motion model only increased from 170.1% CPU usage at 200 entities to 189.8% CPU usage at 2000 entities. For all entity counts, the average CPU usage was higher for the JSBSim motion model than for the two lower fidelity models, with the difference growing smaller as the entity count was increased. 

The median and maximum CPU usage share some trends with the average CPU usage, but deviate in some regards. While both measures generally increase as the entity count is increased, the two lower fidelity models surpass the JSBSim motion model in these measures at higher entity counts. For an entity count of 2000, the Unicycle model has a median CPU usage 196.9% and a max CPU usage of 222.3%, compared to the JSBSim model values of 190.6% and 213.3%. 

These benchmarks demonstrate that, with some restrictions, **it is viable to run SCRIMMAGE in a virtualized environment on a student laptop**. If entity counts aren't pushed too high and a lower fidelity motion model is used, SCRIMMAGE is capable of running missions **many times faster than real time**. This speed means that even time consuming autonomy approaches like reinforcement learning may be viable in a Docker container on modest hardware. Such an approach has the added benefit of requiring little to no setup, as the Docker container comes ready to run SCRIMMAGE. This removes many barriers that might prevent a student from making use of SCRIMMAGE and allows more focus to be given to autonomy development.

## References

1. Demarco, Kevin & Squires, Eric & Day, Michael & Pippin, Charles. (2019). Simulating Collaborative Robots in a Massive Multi-agent Game Environment (SCRIMMAGE). doi://10.1007/978-3-030-05816-6_20. 