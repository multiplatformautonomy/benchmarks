# This Dockerfile is for benchmarking scrimmage
FROM multiplatformautonomy/vscrimmage-vnc
LABEL maintainer="GTRI DFO MASS Project"

# Environment variables
ENV HOME=/home/scrimmage

# Copy Benchmark folders
USER 0
COPY ./scripts $HOME/benchmarks/scripts
COPY ./missions $HOME/benchmarks/missions
RUN chmod +w -R $HOME/benchmarks && chown -R 1000:1000 $HOME/benchmarks

USER 1000
WORKDIR $HOME
