#!/bin/bash
#Runs benchmarks for the same set of conditions as the scrimmage paper.
#Each set of conditions is repeated 5 times.

function cleanup {
    printf "\n\nBenchmark protocol exited!\n\n"
    exit
}

trap cleanup SIGINT

NUMTRIALS=5
motionModels=("Unicycle" "Single Integrator" "JSBSim")
numEntities=(200 500 800 1100 1400 1700 2000)

benchmarksFolder="/home/scrimmage/benchmarks"

#iterate over each combination of motion model and entity count
for i in $(seq $NUMTRIALS); do
    for model in "${motionModels[@]}"; do
        for n in ${numEntities[@]}; do
	    model=$(echo $model | sed 's/ /_/')
	    dirName=${model}_${n}_${i}
	    option="count=$n"

	    $benchmarksFolder/scripts/benchmark.sh -n $dirName -o $option -m $model
	done
    done
done
