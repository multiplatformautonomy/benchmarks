#!/bin/bash
#Outputs a timestamp and RAM usage in kb for the given PID

function ramHelper {
	#getting date 
	d=$(date +%H:%M:%S)
	d=${d//$'\n'/}

	#getting only this process' total usage
	r=$(pmap $1 | tail -n 1 | awk '{print $2}')
	r=${r//'K'}
	printf "$d,$r\n"
}

while true; do
	ramHelper $1
	sleep 1
done	
