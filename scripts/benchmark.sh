#!/bin/bash
#use -n to specify output folder and -o to use scrimmage's variable overrides.
#if the -m tag is used to specify a model, the benchmark will look in ../missions/ for an xml file whose name matches "predator_prey_boids_[model_name].xml
#spaces should be replaced with underscores.

function cleanup { 
	for pid in $scrimmagePID $cpuPID $ramPID; do
	    if [ ! -z $pid ]; then
	        kill $pid
	    fi 
        done

	printf "\nBenchmark stopped!\n"
	exit
}

#killing the spawned processes if we recieve ctrl-c
trap cleanup SIGINT

benchmarksFolder="/home/scrimmage/benchmarks"
scrimmageCommand="scrimmage /home/scrimmage/scrimmage/missions/predator_prey_boids.xml"
outputFolder=""
option=""

#allowing scrimamge -o option to be used. -m can be used to pecify the motion model to use
OPTIND=1
while getopts "o:n:m:" opt; do
	case $opt in
		o) option="-o $OPTARG"	;;
		n) outputFolder=$OPTARG         ;;
		m) scrimmageCommand="scrimmage $benchmarksFolder/missions/predator_prey_boids_$OPTARG.xml"	;;
	esac 
done

printf "\n\trun command: $scrimmageCommand $option\n\n"

#making output folders
if [ ! -d "$benchmarksFolder/results" ]
then
	mkdir $benchmarksFolder/results
fi

if [ ! -d "$benchmarksFolder/results/$outputFolder" ]
then 
	mkdir $benchmarksFolder/results/$outputFolder
fi

#takes system CPU usage. waits five seconds before starting scrimmage to get an idea of background usage
sar -u -P ALL 1 > "$benchmarksFolder/results/$outputFolder/cpuUsage.txt" &
cpuPID=$!

sleep 5

#Running scrimmage with options if they were specified
echo "Starting Benchmark!"
$scrimmageCommand $option &
scrimmagePID=$!

#this script takes the process' RAM usage at a one second interval and attaches a timestamp
echo "Timestamp,RAM(kb)" > "$benchmarksFolder/results/$outputFolder/ramUsage.txt"  
$benchmarksFolder/scripts/outputRamUsage.sh $scrimmagePID >> "$benchmarksFolder/results/$outputFolder/ramUsage.txt" &
ramPID=$!

wait $scrimmagePID

#I'm trying to supress the output from kill, but it doesn't seem like there is a good way to do it...
( kill $cpuPID $ramPID ) &

{
wait $cpuPID
wait $ramPID
} 2>/dev/null

#Copying the generated log file "runtime_seconds.txt"
cp ~/.scrimmage/logs/latest/runtime_seconds.txt $benchmarksFolder/results/$outputFolder/

#formatting cpu usage file
$benchmarksFolder/scripts/formatCpuUsage.py $outputFolder
