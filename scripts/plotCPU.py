#This function reads all the files of a given condition (eg JSBSim_2000), and plots the average time series across trials. Full utilization is [NUM_CORES]*100%

import pandas as pd
import matplotlib.pyplot as plt
import subprocess 
import sys
import re

benchmarksFolder = '/home/scrimmage/benchmarks'

def plot_CPU_averaged(trialName, axis):
    #finding folders that match the condition.
    command = 'dir ' + benchmarksFolder + '/results'
    trials = subprocess.check_output(command, shell=True, universal_newlines=True)
    trials = re.split('[\n\s]+', trials)[:-1]
    trials = [trial for trial in trials if trialName in trial]

    #summing all cores for each trial. 
    cpuUsage = []

    for trial in trials:
        temp = pd.read_csv(benchmarksFolder + '/results/' + trial + '/cpuUsage.txt')
        temp = sum([temp[key] for key in temp if key != 'Timestamp'])
    
        #truncating - need to either truncate this trial, or all the previous ones
        if len(cpuUsage) != 0:
            if len(temp) >= len(cpuUsage[0]):
                temp = temp[0:len(cpuUsage[0])]
            else:
                cpuUsage = [v[0:len(temp)] for v in cpuUsage]
    
        cpuUsage.append(temp)

    cpuUsage = sum(cpuUsage) / len(cpuUsage)
    cpuUsage.plot(ax=axis)
