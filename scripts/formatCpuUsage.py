#!/usr/bin/env python3

#This script formats the raw log cpuUsage.txt file as a csv. It should be called automatically. 

import sys
import subprocess

benchmarksFolder = '/home/scrimmage/benchmarks'

#loading cpu data
fName = benchmarksFolder + '/results/' + sys.argv[1] + '/cpuUsage.txt'
command = "head -n 1 " + fName + " | sed \'s/.*(\([0-9]*\) CPU)/\\1/\'"
numCores = subprocess.check_output(command, shell=True, universal_newlines=True)
numCores = int(numCores)

usage = {}

#reading the core usages and timestamps from the cpuUsage.txt file
for i in range(-1,numCores):
    if i == -1:
        pattern = str(1)
        argNum = str(1)
        key = 'Timestamp'
        func = lambda x: x  #this is bad but I wanted to have all the logic in one place
    else:
        pattern = str(i)
        argNum = str(3)
        key = 'Core ' + str(i)
        func = lambda x: [float(v) for v in x]

    command = "grep \'\\s" + pattern + "\\s\' " + fName + " | awk \'{print $" + argNum + "}\'"
    temp = subprocess.check_output(command, shell=True, universal_newlines=True)
    temp2 = temp.split('\n')[0:-1]

    usage[key] = func(temp2)

#printing to file
f = open(fName, 'w')
header = 'Timestamp'
for i in range(0, numCores):
    header = header + ',' + 'Core ' + str(i)

header = header + '\n'
f.write(header)

for i in range(0, len(usage['Timestamp'])):
    outString=str(usage['Timestamp'][i])
    for j in range(0, numCores):
        outString=outString + ',' + str(usage['Core ' + str(j)][i])

    outString=outString+"\n"
    f.write(outString)

f.close()
