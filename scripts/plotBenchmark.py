#!/usr/bin/env python3
# This script plots the result of a benchmark trial. 
# Usage: ./plotBenchmark.py [benchmark_trial_name]

import pandas as pd
import matplotlib.pyplot as plt
import sys
import subprocess

from datetime import datetime, timedelta

benchmarksFolder = "/home/scrimmage/benchmarks"

#loading elapsed time so we can put it at the top of the figure
command = "awk \'{print $2}\' " + benchmarksFolder + "/results/" + sys.argv[1] + "/runtime_seconds.txt" 
temp = subprocess.check_output(command, shell=True, universal_newlines=True)
temp = temp.split('\n')[0:-1]
walltime = temp[0]
simtime = temp[1]

#loading ram data and converting to GB
ramData = pd.read_csv(benchmarksFolder + '/results/' + sys.argv[1] + '/ramUsage.txt')
ramData['RAM Usage'] = ramData['RAM(kb)'] / 1000000
ramData = ramData.drop(['RAM(kb)'], axis=1)

#need to add a 0 at t0-5s so that the two plots have the same range
ramData['Timestamp'] = pd.to_datetime(ramData['Timestamp'], format='%H:%M:%S')
t = ramData['Timestamp'][0] + timedelta(seconds=-4)
temp = {"Timestamp":[t, ramData['Timestamp'][0]], "RAM Usage":[0, 0]}
temp = pd.DataFrame(data=temp)
ramData = temp.append(ramData, True)

fig = plt.figure()
p1 = fig.add_subplot(311)
p1.set_ylabel("RAM Usage (GB)")
ramData.set_index('Timestamp', inplace=True)

#plotting ram data and elapsed time
p1 = ramData.plot(ax=p1, legend=False)
print("here")
plt.gcf().text(0.5, 0.925, "Wall Time: " + walltime + " s.     Sim Time: " + simtime + "s.", fontsize=14, ha='center')

#loading cpu usage into a DataFrame with the timestamp as the index
cpuData = pd.read_csv(benchmarksFolder + '/results/' + sys.argv[1] + '/cpuUsage.txt')
cpuData['Timestamp'] = pd.to_datetime(cpuData['Timestamp'], format='%H:%M:%S')
cpuData.set_index('Timestamp', inplace=True)


#plotting cpu data
p2 = fig.add_subplot(313)
p2.set_ylabel("CPU usage (%)")
p2 = cpuData.plot(ax=p2, legend=False)

fig.show()

input('Press enter to close')
