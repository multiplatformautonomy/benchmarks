#!/bin/bash
#This script iterates through all the benchmark trials and plots them. 

function cleanup {
	echo "exiting..."
	exit
}

trap cleanup SIGINT

benchmarksFolder="/home/scrimmage/benchmarks"
trials=$(dir "$benchmarksFolder/results")

for trial in $trials; do
	echo "plotting trial: $trial"
	$benchmarksFolder/scripts/plotBenchmark.py $trial
done
