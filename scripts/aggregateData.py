#!/usr/bin/env python3
#This script finds the average ratio of sim time to wall time for each 
#set of conditions. It looks in ../results/ for trials and saves its output
#in ../results_processed/
#TODO: find a good quantity to report for cpu usage

import statistics as stat
import pandas as pd
import subprocess
import os
import re

#writes the output of each func in func_array to the file FName 
#dict_ is a dictionnary where each val is a list
def write_func(fName, header, dict_, func_array):
    f = open(fName, 'w')
    f.write(header)

    for key,val in dict_.items():
        outString = key; 
        for func in func_array:
            outString = outString + ',' + str(func(val))
        outString = outString + '\n'
        f.write(outString)
    
    f.close()

benchmarksFolder = '/home/scrimmage/benchmarks'

#using dir to get an easy to manipulate list of folders
command = 'dir ' + benchmarksFolder + '/results'
trials = subprocess.check_output(command, shell=True, universal_newlines=True)
trials = re.split('[\n\s]+', trials)[:-1]

ratio_dict = {}
ram_dict = {}
cpu_dict = {}

for trial in trials:
    #reading the wall time and sim time from the trial's runtime_seconds.txt file
    fileName = benchmarksFolder + '/results/' + trial + '/runtime_seconds.txt'
    command = 'awk \'{print $2}\' ' + fileName
    vals = subprocess.check_output(command, shell=True, universal_newlines=True)
    vals = re.split('\n', vals)[:-1]
    val_ratio = float(vals[1]) / float(vals[0])

    #Reading ramUsage.txt to find peak ram usage for this trial - this isn't the best use of pandas...
    fileName = benchmarksFolder + '/results/' + trial + '/ramUsage.txt'
    ramData = pd.read_csv(fileName)
    val_ram = max(ramData['RAM(kb)']) / 1000000

    #TODO: reading cpuUsage.txt? Right now I'm just finding the average for all cores and all time
    #This ignores the first 5 seconds where scrimmage isn't running
    fileName = benchmarksFolder + '/results/' + trial + '/cpuUsage.txt'
    cpuData = pd.read_csv(fileName)
    temp = sum([cpuData[key][5:] for key in cpuData if key != 'Timestamp'])
    val_cpu = [stat.mean(temp), stat.median(temp), max(temp)]

    #Entering the values into their respective dictionaries
    key = trial[:-2]
    if key in ratio_dict:
        ratio_dict[key].append(val_ratio)
        ram_dict[key].append(val_ram)
        cpu_dict[key].append(val_cpu)
    else:
        ratio_dict[key] = [val_ratio]
        ram_dict[key] = [val_ram]
        cpu_dict[key] = [val_cpu]

#making the output folder if it doesn't already exist 
outFolder = benchmarksFolder + '/results_processed'
if not os.path.exists(outFolder):
    os.makedirs(outFolder)

#finding the mean and std of the ratio for each set of conditions and writing to a csv
funcs = [stat.mean, stat.stdev]
fName = outFolder + '/average_ratio.txt'
header = 'condition,ratio_avg,ratio_std\n'
write_func(fName, header, ratio_dict, funcs)

#doing the same thing for peak ram usage - peak ram usage of each trial, averaged across trials
fName = outFolder + '/average_peak_ram.txt'
header = 'condition,peak_ram_avg,peak_ram_std\n'
write_func(fName, header, ram_dict, funcs)

#This isn't easy to read... v is a list containing 3 entries - [mean_val median_val max_val] 
#x is a list of such three entry lists, and we are trying to find the average of mean_val, median_val, and
#max_val
funcs = [lambda x,i=i: sum([v[i] for v in x])/len(x) for i in range(0,3)] 
fName = outFolder + '/average_cpu_usage.txt'
header = 'condition,cpu_avg,cpu_median,cpu_max\n'
write_func(fName, header, cpu_dict, funcs)
